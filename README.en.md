# ExcelMerge

#### Description
之前帮朋友东拼西凑的一个Excel合并程序，选中多个".xlsx"文件，将每个Excel的默认sheet0,合并到一个文件里，方便打印
有两种方式供选择，一个是可以勾选需要合并的excel文件来合并到一个Excel，
另外一个是把需要合并的Excel文件名写入一个.txt文件，通过读取这个.txt文件，来合并到一个Excel

#### Software Architecture
Software architecture description

#### Installation

1.  xxxx
2.  xxxx
3.  xxxx

#### Instructions

1.  xxxx
2.  xxxx
3.  xxxx

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request


#### Gitee Feature

1.  You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2.  Gitee blog [blog.gitee.com](https://blog.gitee.com)
3.  Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4.  The most valuable open source project [GVP](https://gitee.com/gvp)
5.  The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6.  The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
