from PyQt5 import QtGui
from PyQt5.QtWidgets import QWidget, QGridLayout, QLabel, QLineEdit, QPushButton, QHBoxLayout, QVBoxLayout

import util

PRINT_SET = 'print_set'
UP_SET = 'up_set'
BOTTOM_SET = 'bottom_set'
LEFT_SET = 'left_set'
RIGHT_SET = 'right_set'
HEADER_SET = 'header_set'
FOOTER_SET = 'footer_set'

DEFAULT_UP_SET = 0.9
DEFAULT_BOTTOM_SET = 0.9
DEFAULT_LEFT_SET = 0.8
DEFAULT_RIGHT_SET = 0.8
DEFAULT_HEADER_SET = 0.8
DEFAULT_FOOTER_SET = 0.8


class PrintSetWidget(QWidget):
    def __init__(self):
        super(PrintSetWidget, self).__init__()
        self.print_footer_text = None
        self.print_header_text = None
        self.print_bottom_text = None
        self.print_left_text = None
        self.print_up_text = None
        self.print_right_text = None
        self.ok_btn = None
        self.cancel_btn = None
        self.setWindowTitle('打印设置')
        self.init_ui()
        self.read_config()

    def init_ui(self):
        v_layout = QVBoxLayout()
        layout = QGridLayout()
        h_layout = QHBoxLayout()
        print_up_btn = QLabel('上(T)')
        self.print_up_text = QLineEdit()
        print_left_btn = QLabel('左(L)')
        self.print_left_text = QLineEdit()
        print_bottom_btn = QLabel('下(B)')
        self.print_bottom_text = QLineEdit()
        print_right_btn = QLabel('右(R)')
        self.print_right_text = QLineEdit()
        header_lb = QLabel('页眉(A)')
        self.print_header_text = QLineEdit()
        footer_lb = QLabel('页脚(F)')
        self.print_footer_text = QLineEdit()
        self.ok_btn = QPushButton('确定')
        self.cancel_btn = QPushButton('取消')
        self.ok_btn.setFixedWidth(100)
        self.cancel_btn.setFixedWidth(100)

        layout.addWidget(QLabel('单位:cm'), 1, 0)
        layout.addWidget(print_up_btn, 1, 2)
        layout.addWidget(self.print_up_text, 1, 3)
        layout.addWidget(header_lb, 1, 6)
        layout.addWidget(self.print_header_text, 1, 7)
        layout.addWidget(print_left_btn, 2, 0)
        layout.addWidget(self.print_left_text, 2, 1)
        layout.addWidget(print_right_btn, 2, 4)
        layout.addWidget(self.print_right_text, 2, 5)
        layout.addWidget(print_bottom_btn, 3, 2)
        layout.addWidget(self.print_bottom_text, 3, 3)
        layout.addWidget(footer_lb, 3, 6)
        layout.addWidget(self.print_footer_text, 3, 7)
        v_layout.addLayout(layout)
        h_layout.addStretch(1)
        h_layout.addWidget(self.ok_btn)
        h_layout.addWidget(self.cancel_btn)
        v_layout.addLayout(h_layout)

        self.print_header_text.setValidator(QtGui.QDoubleValidator())  # 设置只能输入double类型的数据
        self.print_up_text.setValidator(QtGui.QDoubleValidator())  # 设置只能输入double类型的数据
        self.print_bottom_text.setValidator(QtGui.QDoubleValidator())  # 设置只能输入double类型的数据
        self.print_left_text.setValidator(QtGui.QDoubleValidator())  # 设置只能输入double类型的数据
        self.print_right_text.setValidator(QtGui.QDoubleValidator())  # 设置只能输入double类型的数据
        self.print_footer_text.setValidator(QtGui.QDoubleValidator())  # 设置只能输入double类型的数据

        self.setLayout(v_layout)
        self.ok_btn.pressed.connect(self.slot_ok_func)
        self.cancel_btn.pressed.connect(self.close)

    def set_print_settings(self):
        up_set = float(self.print_up_text.text().strip())
        bottom_set = float(self.print_bottom_text.text().strip())
        left_set = float(self.print_left_text.text().strip())
        right_set = float(self.print_right_text.text().strip())
        header_set = float(self.print_header_text.text().strip())
        footer_set = float(self.print_footer_text.text().strip())

        print_set = {
            'up_set': up_set,
            'bottom_set': bottom_set,
            'left_set': left_set,
            'right_set': right_set,
            'header_set': header_set,
            'footer_set': footer_set
        }
        util.write_config({PRINT_SET: print_set})

    def read_config(self):
        print_set = util.get_config(PRINT_SET)
        if not print_set:
            print_set = dict()
        up_set = print_set.get(UP_SET, DEFAULT_UP_SET)
        bottom_set = print_set.get(UP_SET, DEFAULT_BOTTOM_SET)
        left_set = print_set.get(LEFT_SET, DEFAULT_LEFT_SET)
        right_set = print_set.get(RIGHT_SET, DEFAULT_RIGHT_SET)
        header_set = print_set.get(HEADER_SET, DEFAULT_HEADER_SET)
        footer_set = print_set.get(FOOTER_SET, DEFAULT_FOOTER_SET)
        self.print_up_text.setText(up_set.__str__())
        self.print_bottom_text.setText(bottom_set.__str__())
        self.print_left_text.setText(left_set.__str__())
        self.print_right_text.setText(right_set.__str__())
        self.print_footer_text.setText(footer_set.__str__())
        self.print_header_text.setText(header_set.__str__())

    def slot_ok_func(self):
        self.set_print_settings()
        self.close()
