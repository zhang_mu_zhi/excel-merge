from PyQt5 import QtGui
from PyQt5.QtCore import pyqtSignal
from PyQt5.QtGui import QFont, QIcon
from PyQt5.QtWidgets import QWidget, QHBoxLayout, QLabel, QPushButton, QApplication, QFrame

import util
import qtawesome as qta

class TitleWidget(QWidget):
    title_signal = pyqtSignal(int)

    def __init__(self):
        super().__init__()
        self.max_window_flag = False
        self.max_icon = QIcon(qta.icon('fa5.window-maximize'))
        self.restore_icon = QIcon(qta.icon('fa5.window-restore'))
        self.initUI()

    def initUI(self):
        # self.widget.setStyleSheet("QWidget { background-color: #102233 }")
        hbox = QHBoxLayout()
        # title
        self.title_lb = QLabel('Excel_Merge')
        self.title_lb.setFont(QFont("Bauhaus 93", 14))
        self.title_lb.resize(80, 20)
        # self.title_lb.setContentsMargins(10, 0, 0, 0)

        self.filename_lb = QLabel()
        # self.title_lb.setObjectName()
        # 3 btn
        self.close_btn = QPushButton(QIcon(qta.icon('mdi.close')), '')  # 按钮显示显示文本
        self.max_btn = QPushButton(self.max_icon, '')
        # self.max_btn.setStyleSheet("{background-color:#FFFFFF;}")
        self.min_btn = QPushButton(QIcon(qta.icon('fa5.window-minimize')), '')
        # self.frontbtn = QPushButton('ss')
        # self.frontbtn.pressed.connect(self.getFont)
        self.setObjectName('title_wd')

        hbox.addWidget(self.title_lb)
        hbox.addWidget(self.filename_lb)
        # hbox.addWidget(self.frontbtn)
        hbox.addStretch(1)
        hbox.addWidget(self.min_btn)
        hbox.addWidget(self.max_btn)
        hbox.addWidget(self.close_btn)

        # 3. 连接信号槽
        self.close_btn.pressed.connect(self.closeApp)  # 关闭按钮被点击，调用关闭函数
        self.max_btn.pressed.connect(self.emit_sig)  # 最大化按钮被点击，调用最大化_恢复函数
        self.min_btn.pressed.connect(self.emit_sig)  # 最小化按钮被点击，调用最小化函数


        self.setLayout(hbox)

    def closeApp(self):
        app = QApplication.instance()
        app.quit()

    def emit_sig(self):
        sender = self.sender()
        if sender == self.max_btn:
            self.max_window_flag = not self.max_window_flag
            self.title_signal.emit(util.Signal.MAX_SIGNAL)
            if self.max_window_flag:
                self.max_btn.setIcon(self.max_icon)
            else:
                self.max_btn.setIcon(self.restore_icon)
        elif sender == self.min_btn:
            self.title_signal.emit(util.Signal.MIN_SIGNAL)
        else:
            pass

    def mouseDoubleClickEvent(self, a0: QtGui.QMouseEvent) -> None:
        self.title_signal.emit(MAX_SIGNAL)
