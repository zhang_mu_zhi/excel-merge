# ExcelMerge

#### 介绍
之前帮朋友东拼西凑的一个Excel合并程序，选中多个".xlsx"文件，将每个Excel的默认sheet0,合并到一个文件里，方便打印
有两种方式供选择，一个是可以勾选需要合并的excel文件来合并到一个Excel，
另外一个是把需要合并的Excel文件名写入一个.txt文件，通过读取这个.txt文件，来合并到一个Excel

#### 软件架构
软件架构说明
openpyxl
pyqt5
qdarkstyle
logging
json
pathlib

#### 安装教程

1.  xxxx
2.  xxxx
3.  xxxx


#### 使用说明

本软件用于多Excel文件合并默认Sheet
注：仅支持 ".xlsx" 后缀的Excel文件（".xls" 和 ".xlsx" 说明见文档最后Note）
## 使用步骤
1. 选择根目录：此目录或此目录的子目录存放需要合并的Excel
2. 选择合并文件,有2种方式可选
    - 方式1：勾选需要合并的文件，如勾选merge_excel_1.xlsx,merge_excel_2.xlsx....
    - 方式2：或者将要合并的文件写入一个.txt里，以垂直方式写入，每个文件名占一行，如下形式：
        ```text
        merge_excel_1
        merge_excel_2
        merge_excel_3
        ```
3. 选择输出目录：此目录存放合并后的文件
4. 打印设置：此步骤会统一打印的格式，包含上，下，左，右页边距，页眉，页脚的设置，如果不配置，默认配置如下（top=0.3543307(英寸，0.9cm), bottom=0.3543307(英寸，0.9cm), header=0.3149606(英寸，0.8cm),
   footer=0.3149606(英寸，0.8cm), left=0.3149606(英寸，0.8cm),
   right=0.3149606(英寸，0.8cm)）
5. 点击开始，如果路径选择有误，将会提示，如果无误，将开始合并选中的Excel.合并结束会将合并好的文件放入"输出目录/a.xlsx"
6. 合并结束，可通过"打开合并文件"按钮打开合并后的文件，或者通过"打开目标文件夹"打开文件夹
## 界面介绍

## Note

1. （．xls）是03版Office Microsoft Office Excel 工作表的格式，用03版Office，新建Excel默认保存的Excel文件格式的后缀是“．xls”；
2. （．xlsx）是07版Office Microsoft Office Excel 工作表的格式，用07版Office，新建Excel默认保存的的Excel文件格式后缀是“．xlsx”。

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
